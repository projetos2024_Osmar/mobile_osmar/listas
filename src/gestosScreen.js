import React from "react";
import { View, Text, PanResponder, StyleSheet, Dimensions} from "react-native";

const GestosScreen = ({navigation})=>{

    const screenWidth = Dimensions.get('window').width

    const panResponder = React.useRef(

        PanResponder.create({
            onStartShouldSetPanResponder:()=>true,  //Identifica o início do gesto
            onPanResponderMove:(event, gestureState)=>{     //Identifica a execução do gesto(Movimento)
                console.log('Movimento eixo X:', gestureState.moveX, 'Movimento eixo Y:', gestureState.moveY);
                
            },
            onPanResponderRelease: (event, gestureState)=>{     //Identifica o final do gesto
                if(gestureState.dx >screenWidth/2 && gestureState.x0 < 12){  //Verifica se passou da metade da tela
                    navigation.goBack();
                }
            }
        })

    ).current; //Pegar o estado atual do movimento

    return(
        <View style={styles.container} {...panResponder.panHandlers}>
            <Text style={styles.text}>Volta</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#e8e8e8',
    },
    text:{
        fontSize: 20,
        fontWeight: 'bold',
    }
})

export default GestosScreen;