import React from "react";
import { View, Button } from "react-native";
import { useNavigation } from "@react-navigation/native";


const HomeScreen = ()=>{
    const navigation = useNavigation();

    const handleUsers  = () => {
        navigation.navigate('Users');
    }

    const handleGestos = () => {
        navigation.navigate('Gestos');
    }


    return(
        <View>
            <Button title="Ver Usuário" onPress={handleUsers}/>
            <Button title="Ir para aba gestos" onPress={handleGestos}/>
        </View>
    )
};
export default HomeScreen;